<?php

namespace Parse;

use Parse\Rest\Client;

class Batch extends Client
{
    /**
     * Structure:
     *
     * method (mandatory)
     * className (mandatory)
     * id (for method DELETE or PUT)
     * data (for method POST or PUT)
     */
    private $requests = array();
    private $batchSize = 50; // https://parse.com/docs/rest#objects-batch

    public function __construct($batchSize = null)
    {
        parent::__construct();
        if (!empty($batchSize)) {
            $this->batchSize = $batchSize;
        }
    }

    public function setBatchSize($batchSize)
    {
        $this->batchSize = $batchSize;
    }

    public function run()
    {
        $chunks = array_chunk($this->requests, $this->batchSize);
        $responses = array();

        $requestsMade = 0;
        foreach ($chunks as $chunk) {
            $responses[] = $this->request(array(
                'method' => 'POST',
                'requestUrl' => 'batch',
                'data' => array('requests' => $chunk)
            ));
            $requestsMade++;
        }
        return $responses;
    }

    public function addCreate($className, $data)
    {
        $this->addRequest('POST', $className, null, $data);
    }

    public function addUpdate($className, $id, $data)
    {
        $this->addRequest('PUT', $className, $id, $data);
    }

    public function addDelete($className, $id)
    {
        $this->addRequest('DELETE', $className, $id);
    }

    // Array updates require for an operation passed in the data
    public function addArrayAdd($className, $id, $fieldName, $objects)
    {
        $this->addArrayUpdateOperation($className, $id, $fieldName, $objects, 'Add');
    }

    public function addArrayAddUnique($className, $id, $fieldName, $objects)
    {
        $this->addArrayUpdateOperation($className, $id, $fieldName, $objects, 'AddUnique');
    }

    public function addArrayRemove($className, $id, $fieldName, $objects)
    {
        $this->addArrayUpdateOperation($className, $id, $fieldName, $objects, 'Remove');
    }

    private function addArrayUpdateOperation($className, $id, $fieldName, $objects, $operation)
    {
        if (!is_array($objects)) {
            $objects = array($objects);
        }

        $data = array($fieldName => array(
            '__op' => $operation,
            'objects' => $objects
        ));

        $this->addRequest('PUT', $className, $id, $data);
    }

    private function addRequest($method, $className, $id = null, $data = null)
    {
        $path = self::$_parsebasepath . 'classes/' . $className;
        if (!empty($id) && in_array($method, array('PUT', 'DELETE'))) {
            $path .= '/' . $id;
        }

        $request = array(
            'method' => $method,
            'path' =>  $path
        );

        if (!empty($data)) {
            $request['body'] = $data;
        }

        $this->requests[] = $request;
    }
}