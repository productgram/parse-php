<?php

namespace Parse;

use Parse\Rest\Client;

class Config extends Client
{

    public function __construct($class = '')
    {
        parent::__construct();
    }

    public function toObject()
    {
        return json_decode(json_encode($this->data), FALSE);
    }

    public function get(){
            $request = $this->request(
                array(
                    'method' => 'GET',
                    'requestUrl' => 'config/'
                )
            );

            return $request;

    }
}
